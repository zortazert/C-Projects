#include <stdio.h>
#include <time.h>

int main() {
  //Variables
    char title[1000];
    char link[1000];
    char temp;

    //Title
    printf("Title: ");
    scanf("%[^\n]s", title);

    //Title
    printf("Link: ");
    scanf("%c", &temp);
    scanf("%[^\n]s", link);

    //Link
    printf("\nTitle is %s", title);
    printf("\nLink is %s", link);

    //Time
    char cur_time[128];
    
    time_t      t;
    struct tm*  ptm;
    
    t = time(NULL);
    ptm = localtime(&t);
      
    strftime(cur_time, 128, "%a, %d %b %Y %X", ptm);
    
    printf("\nCurrent date and time: %s", cur_time);

  //    time_t t = time(NULL);
  //    struct tm tm = *localtime(&t);
  //    printf("\nTime: %d %d %d %02d:%02d:%02d\n", tm.tm_wday, tm.tm_mon + 1, tm.tm_year + 1900, tm.tm_hour, tm.tm_min, tm.tm_sec);
}
