// Information for C
#include <stdio.h>
// Everything always runs in the main function
int main()
{
  // Declare variable day
  int day;
  // print Day: 
  printf("Day: ");
  // Input is through scanf. We ask for a number so double is used
  scanf("%d", &day);
  // Print out you put: with the variable
  printf("You put: %d", day);
  return 0;
}
